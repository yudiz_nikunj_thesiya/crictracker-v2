/** @type {import('next').NextConfig} */
const nextConfig = {
	reactStrictMode: true,
	images: {
		domains: [
			"crictracker-admin-panel.s3.ap-south-1.amazonaws.com",
			"www.crictracker.com",
			"image.crictracker.com",
		],
		deviceSizes: [450, 992, 1200, 1900],
		imageSizes: [40, 80, 120, 240],
	},
}

module.exports = nextConfig
