import axios from "axios"
import Image from "next/image"
import React, { useEffect, useState } from "react"
import { Container } from "react-bootstrap"
import rankingIcon from "../assets/images/ranking.svg"
import style from "../assets/scss/ranking.module.scss"
import Skeleton, { SkeletonTheme } from "react-loading-skeleton"
import "react-loading-skeleton/dist/skeleton.css"

const Ranking = () => {
	const [rankingData, setRankingData] = useState([])
	const [rankingTab, setRankingTab] = useState("Tests")
	const [rankingSubTab, setRankingSubTab] = useState("Teams")
	const [loading, setLoading] = useState(true)

	useEffect(() => {
		axios
			.post("https://gateway-dev.crictracker.ml/", {
				query: `query GetRankings($input: getRankInput!) {
    getRankings(input: $input) {
      aResults {
        nPoints
        _id
        nRank
        nRating
        sName
        sTeam
        dUpdated
      }
    }
  }`,
				variables: {
					input: {
						eMatchType: rankingTab,
						eRankType: rankingSubTab,
						nLimit: 5,
					},
				},
			})
			.then((res) => {
				setLoading(false)
				setRankingData(res.data?.data?.getRankings?.aResults)
			})
	}, [rankingTab, rankingSubTab])
	return (
		<div className={style.ranking}>
			<div className={style.ranking_headline}>
				<Image src={rankingIcon} objectFit="contain" alt="current Series" />
				<span>Ranking</span>
			</div>
			<div className={style.ranking_container}>
				<div className={style.ranking_tabs}>
					<span
						className={
							rankingTab === "Tests"
								? style.ranking_tab_active
								: style.ranking_tab
						}
						onClick={() => setRankingTab("Tests")}
					>
						Test
					</span>
					<span
						className={
							rankingTab === "Odis"
								? style.ranking_tab_active
								: style.ranking_tab
						}
						onClick={() => setRankingTab("Odis")}
					>
						Odi
					</span>
					<span
						className={
							rankingTab === "T20s"
								? style.ranking_tab_active
								: style.ranking_tab
						}
						onClick={() => setRankingTab("T20s")}
					>
						T20
					</span>
				</div>

				<div className={style.ranking_tabs_links}>
					<span
						className={
							rankingSubTab === "Teams"
								? style.ranking_tabs_link_active
								: style.ranking_tabs_link
						}
						onClick={() => setRankingSubTab("Teams")}
					>
						Team
					</span>
					<span
						className={
							rankingSubTab === "Batsmen"
								? style.ranking_tabs_link_active
								: style.ranking_tabs_link
						}
						onClick={() => setRankingSubTab("Batsmen")}
					>
						Bating
					</span>
					<span
						className={
							rankingSubTab === "Bowlers"
								? style.ranking_tabs_link_active
								: style.ranking_tabs_link
						}
						onClick={() => setRankingSubTab("Bowlers")}
					>
						Bowling
					</span>
					<span
						className={
							rankingSubTab === "AllRounders"
								? style.ranking_tabs_link_active
								: style.ranking_tabs_link
						}
						onClick={() => setRankingSubTab("AllRounders")}
					>
						ALR
					</span>
				</div>

				<div className={style.ranking_table_container}>
					<table className={style.player_heading}>
						<thead>
							<tr>
								<th>Rank</th>
								<th>Player</th>

								<th className={style.player_heading_right}>Rating</th>
							</tr>
						</thead>
						<tbody>
							{loading ? (
								<SkeletonTheme baseColor="#c1c3c8" highlightColor="#a7acb4">
									<tr>
										<td colSpan={3}>
											<Skeleton count={1} />
										</td>
									</tr>
									<tr>
										<td colSpan={3}>
											<Skeleton count={1} />
										</td>
									</tr>
								</SkeletonTheme>
							) : (
								rankingData?.map((item, index) => (
									<tr key={index}>
										<td>{item?.nRank}</td>
										<td>{item?.sName}</td>
										<td className={style.player_heading_right}>
											{item?.nRating}
										</td>
									</tr>
								))
							)}
						</tbody>
					</table>
				</div>
			</div>
		</div>
	)
}

export default Ranking
