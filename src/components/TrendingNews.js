import React, { useEffect, useState } from "react"
import style from "../assets/scss/trendingnews.module.scss"
import fireIcon from "../assets/images/fire.svg"
import Image from "next/image"
import axios from "axios"
import Skeleton, { SkeletonTheme } from "react-loading-skeleton"
import "react-loading-skeleton/dist/skeleton.css"

const TrendingNews = () => {
	const [trendingNewsData, setTrendingNewsData] = useState([])
	const [loading, setLoading] = useState(true)
	useEffect(() => {
		axios
			.post("https://gateway-dev.crictracker.ml/", {
				query: `query GetTrendingNews($input: getTrendingNewsInput!) {
    getTrendingNews(input: $input) {
      nTotal
      aResults {
        sTitle
        oCategory {
          sName
        }
        _id
        dPublishDate
        iArticleId
        nDuration
        oSeo {
          sSlug
        }
      }
    }
  }`,
				variables: {
					input: {
						nSkip: 3,
						nLimit: 4,
					},
				},
			})
			.then((res) => {
				setLoading(false)
				setTrendingNewsData(res?.data?.data?.getTrendingNews?.aResults)
			})
	}, [])
	return (
		<div className={style.trendingnews}>
			<div className={style.trendingnews_headline}>
				<Image src={fireIcon} objectFit="contain" alt="current Series" />
				<span>Trending News</span>
			</div>
			{loading ? (
				<SkeletonTheme baseColor="#c1c3c8" highlightColor="#a7acb4">
					<h4>
						<Skeleton width="40%" count={1} />
					</h4>
					<p>
						<Skeleton count={2} />
					</p>
					<h4>
						<Skeleton width="40%" count={1} />
					</h4>
					<p>
						<Skeleton count={2} />
					</p>
				</SkeletonTheme>
			) : (
				<div className={style.trendingnews_list}>
					{trendingNewsData.map((item, index) => (
						<div key={index} className={style.news}>
							<span className={style.number}>{index + 1}</span>
							<div className={style.desc}>
								<span className={style.lable}>{item?.oCategory?.sName}</span>
								<span className={style.title}>{item?.sTitle}</span>
							</div>
						</div>
					))}
				</div>
			)}
		</div>
	)
}

export default TrendingNews
