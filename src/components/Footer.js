import React from "react"
import style from "../assets/scss/footer.module.scss"
import logo from "../assets/images/cric.webp"
import appstore from "../assets/images/appstore.svg"
import googleplay from "../assets/images/googleplay.svg"
import facebook from "../assets/images/facebook.svg"
import instagram from "../assets/images/instagram.svg"
import twitter from "../assets/images/twitter.svg"
import linkedin from "../assets/images/linkedin.svg"
import telegram from "../assets/images/telegram.svg"
import youtube from "../assets/images/youtube.svg"
import Image from "next/image"

const Footer = () => {
	return (
		<div className={style.footer}>
			<div className={style.footer_sections}>
				<div className={style.footer_topSeries}>
					<div className={style.footer_heading}>
						<div className={style.footer_heading_line}></div>
						<div className={style.footer_heading_lbl}>
							<span className={style.lbl}>TOP SERIES</span>
						</div>
						<div className={style.footer_heading_line}></div>
					</div>
					<div className={style.footer_links}>
						<span className={style.footer_link}>England v Ireland</span>
						<span className={style.footer_link}>England v Pakistan</span>
						<span className={style.footer_link}>England v West Indies</span>
						<span className={style.footer_link}>CPL</span>
						<span className={style.footer_link}>IPL</span>
						<span className={style.footer_link}>Finnish Premier League</span>
						<span className={style.footer_link}>World Test Championship</span>
						<span className={style.footer_link}>BBL 2021</span>
						<span className={style.footer_link}>County Championship</span>
						<span className={style.footer_link}>Europe 20-20 League</span>
					</div>
				</div>
				<div className={style.footer_topPlayers}>
					<div className={style.footer_heading}>
						<div className={style.footer_heading_line}></div>
						<div className={style.footer_heading_lbl}>
							<span className={style.lbl}>TOP PLAYERS</span>
						</div>
						<div className={style.footer_heading_line}></div>
					</div>
					<div className={style.footer_links}>
						<span className={style.footer_link}>Anatoliy Shcherbatykh</span>
						<span className={style.footer_link}>Chikanso Chima</span>
						<span className={style.footer_link}>Fatima Delgadillo</span>
						<span className={style.footer_link}>Jaroslav Brabec</span>
						<span className={style.footer_link}>Lucy Miller</span>
						<span className={style.footer_link}>Lungelo Ngcaba</span>
						<span className={style.footer_link}>Marti Valencia</span>
						<span className={style.footer_link}>Pin Jung-Eum</span>
						<span className={style.footer_link}>Tikhon Yaroslavsky</span>
						<span className={style.footer_link}>Tum Tantasatityanon</span>
					</div>
				</div>
				<div className={style.footer_topTeam}>
					<div className={style.footer_heading}>
						<div className={style.footer_heading_line}></div>
						<div className={style.footer_heading_lbl}>
							<span className={style.lbl}>TOP TEAM</span>
						</div>
						<div className={style.footer_heading_line}></div>
					</div>
					<div className={style.footer_links}>
						<span className={style.footer_link}>India</span>
						<span className={style.footer_link}>Australia</span>
						<span className={style.footer_link}>England</span>
						<span className={style.footer_link}>New Zeland</span>
						<span className={style.footer_link}>Pakistan</span>
						<span className={style.footer_link}>West Indies</span>
						<span className={style.footer_link}>South Africa</span>
						<span className={style.footer_link}>Afghanistan</span>
						<span className={style.footer_link}>Sri Lanka</span>
						<span className={style.footer_link}>Ireland</span>
					</div>
				</div>
				<div className={style.footer_more}>
					<div className={style.footer_heading}>
						<div className={style.footer_heading_line}></div>
						<div className={style.footer_heading_lbl}>
							<span className={style.lbl}>MORE</span>
						</div>
						<div className={style.footer_heading_line}></div>
					</div>
					<div className={style.footer_links}>
						<span className={style.footer_link}>What is Cricket</span>
						<span className={style.footer_link}>Ranking</span>
						<span className={style.footer_link}>Interviews</span>
						<span className={style.footer_link}>Editors Pick</span>
						<span className={style.footer_link}>Social Tracker</span>
						<span className={style.footer_link}>Cricket Appeal</span>
						<span className={style.footer_link}>Cricket Videos</span>
						<span className={style.footer_link}>Other Sports</span>
						<span className={style.footer_link}>GDPR Compliance</span>
						<span className={style.footer_link}>Affiliate</span>
					</div>
				</div>
			</div>
			<div className={style.footer_divider}></div>
			<div className={style.footer_bottom}>
				<div className={style.footer_bottom_up}>
					<Image src={logo} alt="logo" width={176} objectFit="contain" />
					<div className={style.footer_nav}>
						<span>About</span>
						<span>Contact</span>
						<span>Careers</span>
						<span>Advertise with Us</span>
						<span>Write For Us</span>
						<span>DMCA</span>
						<span>Disclaimer</span>
					</div>
				</div>

				<div className={style.footer_bottom_middle}>
					<div className={style.footer_bottom_middle__left}>
						<Image src={appstore} alt="logo" objectFit="contain" />
						<Image src={googleplay} alt="logo" objectFit="contain" />
					</div>
					<div className={style.footer_social_nav}>
						<Image src={facebook} alt="logo" objectFit="contain" />
						<Image src={instagram} alt="logo" objectFit="contain" />
						<Image src={twitter} alt="logo" objectFit="contain" />
						<Image src={linkedin} alt="logo" objectFit="contain" />
						<Image src={telegram} alt="logo" objectFit="contain" />
						<Image src={youtube} alt="logo" objectFit="contain" />
					</div>
				</div>
				<div className={style.footer_bottom_down}>
					<div className={style.footer_bottom_down__left}>
						© 2013-2021 CricTracker Pvt Ltd All rights reserved.
					</div>
					<div className={style.footer_nav}>
						<span>Terms and Conditions</span>
						<span>Privacy Policy</span>
						<span>Copyrights Notice</span>
					</div>
				</div>
			</div>
		</div>
	)
}

export default Footer
