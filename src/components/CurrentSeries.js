import Image from "next/image"
import React, { useEffect, useState } from "react"
import style from "../assets/scss/currentseries.module.scss"
import troffy from "../assets/images/troffy.svg"
import axios from "axios"
import Skeleton, { SkeletonTheme } from "react-loading-skeleton"
import "react-loading-skeleton/dist/skeleton.css"

const CurrentSeries = () => {
	const [currentSeriesData, setCurrentSeriesData] = useState([])
	const [loading, setLoading] = useState(true)
	useEffect(() => {
		axios
			.post("https://gateway-dev.crictracker.ml/", {
				query: `query GetCurrentPopularSeries {
    getCurrentPopularSeries {
      aResults {
        _id
        oSeries {
          oCategory {
            oSeo {
              sSlug
            }
          }
          oSeo {
            sSlug
          }
          sTitle
        }
      }
    }
  }`,
			})
			.then((res) => {
				setLoading(false)
				setCurrentSeriesData(res.data?.data?.getCurrentPopularSeries?.aResults)
			})
	}, [])
	return (
		<div className={style.currentseries}>
			<div className={style.currentseries_headline}>
				<Image src={troffy} objectFit="contain" alt="current Series" />
				<span>Current Series</span>
			</div>
			{loading ? (
				<SkeletonTheme baseColor="#c1c3c8" highlightColor="#50555d">
					<div className={style.currentseries_tabs}>
						<Skeleton count={1} className={style.currentseries_tab} />
						<Skeleton count={1} className={style.currentseries_tab} />
						<Skeleton count={1} className={style.currentseries_tab} />
						<Skeleton count={1} className={style.currentseries_tab} />
						<Skeleton count={1} className={style.currentseries_tab} />
					</div>
				</SkeletonTheme>
			) : (
				<div className={style.currentseries_tabs}>
					{currentSeriesData.map((item) => (
						<div key={item?._id} className={style.currentseries_tab}>
							<div className={style.currentseries_tab_left}>
								<span>{item?.oSeries?.sTitle}</span>
							</div>
						</div>
					))}
				</div>
			)}
		</div>
	)
}

export default CurrentSeries
