import Image from "next/image"
import React from "react"
import FileIcon from "../assets/images/file.svg"
import FileIconDark from "../assets/images/article_dark.svg"
import FantasyIcon from "../assets/images/fantasy.svg"
import FantasyWhiteIcon from "../assets/images/fantacy_white.svg"
import VideoIcon from "../assets/images/video.svg"
import VideoWhiteIcon from "../assets/images/video_white.svg"
import style from "../assets/scss/tabpane.module.scss"

const TabPane = ({ tabPane, setTabPane }) => {
	return (
		<div className={style.tabpane}>
			<div
				className={
					tabPane === "articles" ? style.tabpane_tab__active : style.tabpane_tab
				}
				onClick={() => {
					setTabPane("articles")
				}}
			>
				<div className={style.tabpane_icon}>
					<Image
						src={tabPane === "articles" ? FileIcon : FileIconDark}
						alt="Articles"
						objectFit="contain"
					/>
				</div>

				<span>Articles</span>
			</div>
			<div
				className={
					tabPane === "fantacy" ? style.tabpane_tab__active : style.tabpane_tab
				}
				onClick={() => {
					setTabPane("fantacy")
				}}
			>
				<div className={style.tabpane_icon}>
					<Image
						src={tabPane === "fantacy" ? FantasyWhiteIcon : FantasyIcon}
						alt="Articles"
						objectFit="contain"
					/>
				</div>

				<span>Fantasy</span>
			</div>

			<div
				className={
					tabPane === "videos" ? style.tabpane_tab__active : style.tabpane_tab
				}
				onClick={() => {
					setTabPane("videos")
				}}
			>
				<div className={style.tabpane_icon}>
					<Image
						src={tabPane === "videos" ? VideoWhiteIcon : VideoIcon}
						objectFit="contain"
						alt="Articles"
					/>
				</div>

				<span>Videos</span>
			</div>
		</div>
	)
}

export default TabPane
