import Image from "next/image"
import React, { useEffect, useState } from "react"
// import CricSpecialContent from "./CricSpecialContent"
import postImg from "../assets/images/Kevin-Pietersen.webp"
import style from "../assets/scss/cricspecial.module.scss"
import StarFilled from "../assets/images/Star_filled.svg"
import Carousel from "react-bootstrap/Carousel"
import axios from "axios"
import Skeleton, { SkeletonTheme } from "react-loading-skeleton"

const CricSpecial = () => {
	const [cricSpecialData, setCricSpecialData] = useState([])
	const [loading, setLoading] = useState(true)

	useEffect(() => {
		axios
			.post("https://gateway-dev.crictracker.ml/", {
				query: `query GetCricSpecial($input: getCricspecialInput!) {
    getCricSpecial(input: $input) {
      nTotal
      aResults {
        _id
        iArticleId
        dPublishDate
        sTitle
        nDuration
        oImg {
          sUrl
          sText
          sAttribute
          sCaption
        }
        oTImg {
          sUrl
          sText
          sCaption
          sAttribute
        }
        oCategory {
          sName
        }
        oSeo {
          sSlug
        }
      }
    }
  }`,
				variables: {
					input: {
						nSkip: 1,
						nLimit: 10,
					},
				},
			})
			.then((res) => {
				setLoading(false)
				setCricSpecialData(res.data?.data?.getCricSpecial?.aResults)
			})
	}, [])

	return (
		<div className={`${style.cricSpecial}`}>
			<h3 className={style.cricSpecial_heading}>
				<Image src={StarFilled} alt="trending" />
				<span>CricSpecial</span>
			</h3>
			{loading ? (
				<SkeletonTheme baseColor="#c1c3c8" highlightColor="#50555d">
					<div className={style.slider}>
						<Skeleton count={1} width="100%" height="150px" />
						<Skeleton count={1} />
						<Skeleton count={1} />
					</div>
				</SkeletonTheme>
			) : (
				<Carousel
					indicators={false}
					controls={false}
					touch={true}
					interval={3000}
					className={style.slider}
				>
					{cricSpecialData.map((item) => (
						<Carousel.Item key={item?._id} className={style.slider_item}>
							{/* <div className={style.slider_image}> */}
							<Image
								src={
									item?.oImg?.sUrl.includes("https://www.crictracker.com/")
										? item?.oImg?.sUrl
										: `https://crictracker-admin-panel.s3.ap-south-1.amazonaws.com/${item?.oImg?.sUrl}`
								}
								onError={({ currentTarget }) => {
									currentTarget.onerror = null // prevents looping
									currentTarget.src =
										"https://image.crictracker.com/wp-content/uploads/2022/04/Delhi-Capitals-5.jpg"
								}}
								alt="post"
								width={266}
								height={162}
								layout="responsive"
								objectFit="cover"
								className={style.slider_image}
							/>
							{/* </div> */}
							<div className={style.slider_label}>
								<span>{item?.oCategory?.sName}s</span>
							</div>
							<div className={style.slider_desc}>{item?.sTitle}</div>
						</Carousel.Item>
					))}
				</Carousel>
			)}
		</div>
	)
}

export default CricSpecial
