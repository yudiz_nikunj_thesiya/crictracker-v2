import Image from "next/image"
import React from "react"
import liveicon from "../assets/images/ic_live.svg"
import downarrow from "../assets/images/ic_drop_down.svg"
import style from "../assets/scss/header.module.scss"
import ScrollContainer from "react-indiana-drag-scroll"
import Link from "next/link"

const Header = () => {
	return (
		<div className={style.header}>
			<ScrollContainer horizontal={true} className={style.header_container}>
				<div>
					<Image src={liveicon} alt="live" />
					<Link href="/categories">
						<span>Live Updates</span>
					</Link>
				</div>
				<Link href="/details">
					<span className={style.active}>Ispanl 2021</span>
				</Link>
				<span>Fantasy Tips</span>
				<span>Teams</span>
				<span>News</span>
				<span>Fixture</span>
				<span>Stats</span>
				<span>Videos</span>
				<span>World Cup 2022</span>
				<span>ICC Awards</span>
				<span>Cpl 2021</span>
				<div>
					<span>More</span>
					<Image src={downarrow} alt="live" />
				</div>
			</ScrollContainer>
		</div>
	)
}

export default Header
