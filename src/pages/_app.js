import { useRouter } from "next/router"
import { useEffect, useState } from "react"
import { ThemeProvider } from "react-bootstrap"
import { BeatLoader, MoonLoader } from "react-spinners"
import "../assets/scss/globals.scss"
import "bootstrap/dist/css/bootstrap.min.css"
import Layout from "../components/Layout"

function MyApp({ Component, pageProps }) {
	const router = useRouter()
	const [loading, setLoading] = useState(false)

	useEffect(() => {
		const handleStart = (url) => {
			setLoading(true)
		}
		const handleStop = () => {
			setLoading(false)
		}

		router.events.on("routeChangeStart", handleStart)
		router.events.on("routeChangeComplete", handleStop)
		router.events.on("routeChangeError", handleStop)

		return () => {
			router.events.off("routeChangeStart", handleStart)
			router.events.off("routeChangeComplete", handleStop)
			router.events.off("routeChangeError", handleStop)
		}
	}, [router])

	return loading ? (
		<div className="transition">
			<MoonLoader color="#045de9" loading={loading} size={50} />
		</div>
	) : (
		<Layout>
			<ThemeProvider
				breakpoints={["xxxl", "xxl", "xl", "lg", "md", "sm", "xs", "xxs"]}
			>
				<Component {...pageProps} />
			</ThemeProvider>
		</Layout>
	)
}

export default MyApp
